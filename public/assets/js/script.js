
// Number 1

function numDivisible (num) {
	if (num % 5 == 0 && num % 3== 0){
		return "FizzBuzz";
	}else if (num % 5 == 0){
		return "Buzz";
	}else if ( num % 3== 0){
		return "Fizz";
	}else 
		return "Pop";	
}

console.log (numDivisible(5));
console.log (numDivisible(30));
console.log (numDivisible(27));
console.log (numDivisible(17));


// Number 2 
function color(letter){
	let colorLetter = letter.toLowerCase();

	if (colorLetter === "r"){
		return "Red";
	}else if (colorLetter === "o"){
		return "Orange";
	}else if (colorLetter === "y"){
		return "Yellow";
	}else if (colorLetter === "g"){
		return "Green";
	}else if (colorLetter === "b"){
		return	"Blue";
	}else if (colorLetter === "i"){
		return "Indigo";
	}else if (colorLetter === "v"){
		return ("Violet");
	}else{
		return " No color";
	}
}

console.log (color("r"));
console.log (color("G"));
console.log (color("x"));
console.log (color("B"));
console.log (color("i"));

// Number 3

function leapYear (year) {
	if (year % 4 == 0){
		return("Leap Year");
	}else if (year % 100 == 400){
		return("Leap Year");
	}else
		return ("Not Leap Year");
}

console.log(leapYear(1900));
console.log(leapYear(2000));
console.log(leapYear(2004));
console.log(leapYear(2021));

